import React, { Component, Fragment } from 'react';
import Homepage from './components/Homepage';
import Header from './components/Header';
import Footer from './components/Footer';
import './App.css';

class App extends Component {
  render() {
    return (
        <Fragment>
          <Header />
          <Homepage />
          <Footer />
        </Fragment>
    );
  }
}

export default App;
