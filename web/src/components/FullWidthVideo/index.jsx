import React from 'react';

export default FullWidthVideo = ({ youtubeId }) => (
    <section id="video" className="video-section">
        <div className="container">
            <div className="row">
                <div className="col-md-12 col-lg-12 video-text">
                    <div className="d-table">
                        <div className="banner-play-button">
                            <a href={`https://www.youtube.com/watch?v=mqwum4p60uQ${youtubeId}`} id="btn"
                               className="btn play-btn video-play wow animated fadeInUp" data-wow-delay="0.5s"
                               data-wow-duration="0.5s">
                                <span className="btn-icon">
                              <i className="fa fa-play"></i>
                             </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
)