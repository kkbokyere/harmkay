import React from 'react';
import jeff_image_1 from '../../images/jb.jpg';

const About = () => 
<section id="about" className="about-section">
    <div className="container">
        <div className="row">
            <div className="col-md-12 about-text">
                <h3>About Me</h3>
                <p>UK Host and radio presenter. Harm Kay is known for his energetic and cheeky characteristic when on stage. His crowd engagement gives him the ability to not only entertain an audience but to keep them guessing at all times with what he'll do next. Born in the UK, but raised in a Ghanaian home Harm Kay holds his heritage close to his heart and continues to promote and emphasise on the Ghanaian values and culture through his works.</p>
                <p>Harm Kay does many private gigs including weddings and club appearances. An area Harm Kay takes a personal interest in, is giving talks/seminars to university students. His seminar <strong>"Life after University"</strong> has become one of the most popular seminars out of the national curriculum.</p>
                <p>Very well known in the Ghanaian community Harm Kay has hosted some major events including:</p>
                <ul className="about-events">
                    <li>Ghana Escapes (Accra)</li>
                    <li>Ghana Party in the Park (Trent Park-UK)</li>
                    <li>Shatta Wale Concert (Indigo O2-UK)</li>
                    <li>Bisa Kdei (Camden Centre-UK)</li>
                    <li>Wahala Comedy Show (Indigo O2-UK)</li>
                    <li>Ghana Independence (The O2-UK)</li>
                    <li>Africa Live (Catford Broadway Theatre UK)</li>
                    <li>Afrobeat Weekender (Skegness UK)</li>
                    <li>Afrobeat Sunday (Coronet-UK)</li>
                    <li>Sarkodie (Catford Broadway Theatre-UK)</li>
                </ul>
            </div>
        </div>
    </div>
</section>;

export default About;