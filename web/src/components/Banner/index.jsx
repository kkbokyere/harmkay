import React from 'react';
import PropTypes from 'prop-types';

const Banner = ({ bodyContent, image, ctaButtonText, ctaButtonUrl }) => {
    return (
        <section id="home-banner" className="banner-section wow fadeInUp parallax" data-wow-delay="0.4s" style={{
            backgroundImage: `url(${image})`,
        }}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-lg-12 banner-text wow fadeInUp" data-wow-delay="0.4s">
                        {bodyContent}
                        {ctaButtonUrl && <a className="banner-btn" target="_blank" href={ctaButtonUrl}>{ctaButtonText}</a>}
                    </div>
                </div>
            </div>
        </section>
    )
};


Banner.propTypes = {
    image: PropTypes.string,
    ctaButtonText: PropTypes.string,
    ctaButtonUrl:PropTypes.string,
    bodyContent: PropTypes.any
};

export default Banner;