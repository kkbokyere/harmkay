import React, { Component, Fragment } from 'react';

import About from '../About';
import VideoGrid from '../VideoGrid';
import VideoGridItem from '../VideoGrid/VideoGridItem';
import Contact from '../Contact';
import Banner from '../Banner';
import Events from '../Events';
import { Slider, SliderItem, SliderImage } from '../Slider';
import EventItem from '../Events/EventItem';
import homepagewallpaper1 from '../../images/homepagewallpaper1.jpeg';
import homepagewallpaper2 from '../../images/homepagewallpaper2.jpeg';
import jeff_image_1 from '../../images/jeffrey-pic.jpeg';
import ghanaescapes from '../../images/ghanaescapes2.jpg';

class Homepage extends Component {

    render() {
        return (
            <Fragment>
                {/*<SliderVideo youtubeId="mqwum4p60uQ"/>*/}
                <SliderImage
                    bodyContent={<h1>UK Host & Radio Presenter</h1>}
                    image={homepagewallpaper1}
                    ctaButtonText="Find out more"
                    ctaButtonUrl="#about"
                />
                <About />
                <VideoGrid
                    title="Media"
                    subtitle="COMING SOON"
                >
                </VideoGrid>
                <Events>
                    <EventItem
                        location="Amsterdam"
                        title="Afrobeat"
                        date="2018-11-24"
                    />
                    <EventItem
                        location="Double Tree Hilton"
                        title="Mental Health Awareness Gala"
                        date="2018-11-24"
                    />
                    <EventItem
                        location="West End"
                        title="Private 30th Birthday Dinner & Dance"
                        date="2018-12-01"
                    />
                    <EventItem
                        location="Aldgate"
                        title="Harm Kay's Christmas Party"
                        date="2018-12-21"
                    />
                    <EventItem
                        location="East Legon, Ghana"
                        url="http://www.ghanaescapes.com/"
                        externalLink
                        title="Daytox"
                        date="2018-12-27"
                    />
                    <EventItem
                        location="Ghana"
                        url="http://www.ghanaescapes.com/"
                        externalLink
                        title="All Black Party"
                        date="2018-12-28"
                    />
                    <EventItem
                        location="Ghana"
                        url="http://www.ghanaescapes.com/"
                        externalLink
                        title="Rep Your Country Pool Party"
                        date="2018-12-30"
                    />
                </Events>
                <Banner
                    image={ghanaescapes}
                    bodyContent={<div><h1>Ghana Escapes</h1><p>Ghana's Number One Holiday Package</p></div>}
                    ctaButtonUrl="http://www.ghanaescapes.com"
                    ctaButtonText="Ghana Escapes"
                />
                <Contact />
            </Fragment>
        )
    }
}

export default Homepage;