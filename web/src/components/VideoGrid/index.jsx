import React from 'react';
import mediaBg from '../../images/mediabg.jpeg'
const VideoGrid = ({ title, subtitle, children }) => {
    const content = Array.isArray(children) ? children : [children];
    return (
        <section id="media" className="services-section parallax" style={{
            backgroundImage: `url(${mediaBg})`,
        }}>
            <div className="container">
                <div className="row wow fadeIn" data-wow-delay="0.2s">
                    <div className="col-md-12 col-sm-12 services-heading">
                        <h2>{title}</h2>
                        <p>{subtitle}</p>
                    </div>
                    {
                        content.map((item, i) => <div key={i} className="col-lg-4 col-md-6">{item}</div>)
                    }
                </div>
            </div>
        </section>
    )
};

export default VideoGrid;