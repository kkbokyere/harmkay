import React from 'react';

const VideoGridItem = ({ imgSrc, altText }) => {
    return (
        <div className="services-text">
            <div className="services-content">
                <img src={imgSrc} alt={altText} />
            </div>
        </div>
    )
};

export default VideoGridItem;