import React from 'react';

const Footer = () => (
    <footer id="footer" className="footer-section">
        <div className="container copyright-wrapper">
            <div className="row">
                <div className="col-md-12 copyright-social text-center">
                    <a href="https://www.facebook.com/Harm-Kay-UKGhanaian-Presenter-657806164587443/" target="_blank"><i className="fa fa-facebook"></i></a>
                                    <a href="http://www.instagram.com/Harm_Kay"><i className="fa fa-instagram"></i></a>
                                    <a href="mailto:bookings@harmkay.com"><i className="fa fa-envelope"></i></a>
                </div>
                <div className="col-md-12 copyright-text text-center">
                    <p>&copy; Copyright 2018 <a href="/">harmkay.com</a> All Rights Reserved.</p>
                    <p>Site by <a href="http://www.nowahala.co.uk" target="_blank">NoWahala</a>.</p>
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;