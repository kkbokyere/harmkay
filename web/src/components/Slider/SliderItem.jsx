import React from 'react';
import PropTypes from 'prop-types';

const SliderItem = ({ image, ctaButtonText, ctaButtonUrl, bodyContent }) => {
    return (
        <div className="single-slider" style={{
            backgroundImage: `url(${image})`,
        }}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-lg-12 banner-text wow fadeInUp">
                        {bodyContent}
                        {ctaButtonUrl && <a className="banner-btn" href={ctaButtonUrl}>{ctaButtonText}</a>}
                    </div>
                </div>
            </div>
        </div>
    )
};
SliderItem.propTypes = {
    image: PropTypes.string,
    ctaButtonText: PropTypes.string,
    ctaButtonUrl:PropTypes.string,
    bodyContent: PropTypes.any
};
export default SliderItem;