import React from 'react';
import PropTypes from 'prop-types';
const SliderVideo = ({ youtubeId, ctaButtonText, ctaButtonUrl, bodyContent}) => {
    return (
        <section id="video-bg" className="youtube-video-overlay">
            <div id="ytvideo" className="player" data-property={`
                {videoURL:'https://www.youtube.com/watch?v=${youtubeId}', containment:'#video-bg', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, addRaster:true, quality:'default'}
            `}></div>
            <div className="container" style={{
                zIndex:100
            }}>
                <div className="row">
                    <div className="col-md-12 col-lg-12 banner-text wow fadeInUp" data-wow-delay="0.4s">
                        {bodyContent}
                        {ctaButtonUrl && <a className="banner-btn" href={ctaButtonUrl}>{ctaButtonText}</a>}
                    </div>
                </div>
            </div>
        </section>
    )
};

SliderVideo.propTypes = {
    youtubeId: PropTypes.string,
    bodyContent: PropTypes.any,
    ctaButtonText: PropTypes.any,
    ctaButtonUrl: PropTypes.string,
};

export default SliderVideo;