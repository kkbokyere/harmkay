import Slider from './Slider';
import SliderItem from './SliderItem';
import SliderVideo from './SliderVideo';
import SliderImage from './SliderImage';

export {
    Slider,
    SliderItem,
    SliderVideo,
    SliderImage,
};