import React from 'react';
 
const Slider = ({ children }) => {
    return(
        <section className="slider-section">
            <div className="carousel-single-item">
                {children}
            </div>
        </section>
    )
};

export default Slider;