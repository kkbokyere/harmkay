import React, { Component } from 'react';
import logo from '../../images/header-banner/hk-logo.png';
import logoText from '../../images/header-banner/harm-kay-text.png';

class Header extends Component {
    render() {
       return (
           <div>
               <div className="preloader">
                    <div className="spinner">
                        <div className="bounce1" />
                        <div className="bounce2" />
                    </div>
                </div>
                <div className="scrollup">
                    <i aria-hidden="true" className="fa fa-chevron-up" />
                </div>
                <div id="home" />
                <header id="site-header" className="home-header">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2 header-logo">
                                <a href="#home">
                                    <span className="main-logo"><img src={logo} alt="Harm Kay Logo"/></span>
                                    <span className="shorten-logo"><img src={logoText} alt="Harm Kay Logo"/></span>
                                </a>
                            </div>
                            <div className="col-md-10">
                                <div className="main-menu-area">
                                    <nav className="header-nav">
                                        <ul id="navigation">
                                            <li className="active">
                                                <a href="#home">Home</a>
                                            </li>
                                            <li>
                                                <a href="#about">About</a>
                                            </li>  
                                            <li>
                                                <a href="#events">Events</a>
                                            </li>
                                                                        
                                            <li>
                                                <a href="#media">Media</a>
                                            </li>
                                            <li>
                                                <a href="#contact">Contact</a>
                                            </li>
                                            <li>
                                                <a href="http://www.ghanaescapes.com" target="_blank">Ghana Escapes</a>
                                            </li>
                                            <li>
                                                <a href="https://www.facebook.com/Harm-Kay-UKGhanaian-Presenter-657806164587443/" target="_blank"><i className="fa fa-facebook" /></a>
                                            </li>
                                            <li><a href="http://www.instagram.com/Harm_Kay"><i className="fa fa-instagram" /></a></li>
                                            <li><a href="mailto:bookings@harmkay.com"><i className="fa fa-envelope" /></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="main-mobile-menu">
                                    <nav id="mobile-nav">
                                        <ul id="mobile-navigation" className="mobilenav">
                                            <li>
                                                <a className="active" href="#home">Home</a>
                                            </li>
                                            <li>
                                                <a href="#about">About</a>
                                            </li>
                                            <li>
                                                <a href="#events">Events</a>
                                            </li>
                                            <li>
                                                <a href="#media">Media</a>
                                            </li>

                                            <li>
                                                <a href="#contact">Contact</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
           </div>
        )
    }
}

export default Header;