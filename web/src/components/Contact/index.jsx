import React from 'react';

const Contact = () => (
    <section id="contact" className="contact-section">
        <div className="container">
            <div className="row">
                <div className="col-md-12 col-sm-12 contact-heading">
                    <h2>Contact</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-md-3 contact-text">
                    <div className="contact-text-details">
                        <h4>Bookings</h4>
                        <div className="contact-text-details-content">
                            <span>
                                <p>Melissa Caramba-Coker</p>
                                <p><a href="mailto:bookings@harmkay.com">bookings@harmkay.com</a></p>
                                <p><a href="tel:+44 7415753895">+44 7415753895</a></p>
                            </span>
                        </div>
                    </div>

                </div>
                <div className="col-md-3 contact-text">
                    <div className="contact-text-details">
                        <h4>Head Office</h4>
                        <div className="contact-text-details-content">
                            <p>
                                HK Productions Events
                                <br/>
                                160 City Road
                                <br/>
                                Kemp House
                                <br/>
                                London
                                <br/>
                                EC1V 2NX
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 contact-text">
                    <div className="contact-text-details">
                        <h4>Ghana Escapes</h4>
                        <div className="contact-text-details-content">
                            <p>
                                <a href="http://www.ghanaescapes.com" target="_blank">Ghana Escapes</a>
                            </p>
                        </div>
                    </div>

                </div>
                <div className="col-md-3 contact-text">
                    <div className="contact-text-details">
                        <h4>Merchandise</h4>
                        <div className="contact-text-details-content">
                            <p><a href="http://www.ogldn.com" target="_blank">OGLDN</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

export default Contact;