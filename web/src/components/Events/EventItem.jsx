import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const EventItem = ({ title, location, url, date, time, externalLink }) => {
    const eventDate = moment(date);
    return (
        <article className="eventlist-event eventlist-event--upcoming">
            <div>
                <a href={url} className="eventlist-column-date">

                    <div className="eventlist-datetag">

                        <div className="eventlist-datetag-inner">
                            <div className="eventlist-datetag-startdate eventlist-datetag-startdate--day">{eventDate.format('Do')}</div>
                            <div className="eventlist-datetag-startdate eventlist-datetag-startdate--month">{eventDate.format('MMM')}</div>
                            <div className="eventlist-datetag-startdate eventlist-datetag-startdate--month">{eventDate.format('YYYY')}</div>
                            {time && <div className="eventlist-datetag-time"><span className="event-time-24hr">{eventDate.format('hh:mm:a')}</span></div>}

                            <div className="eventlist-datetag-status"></div>

                        </div>

                    </div>

                </a>

                <div className="eventlist-column-info">

                    <h1 className="eventlist-title">
                        {url ? <a href={url} target={`${ externalLink ? '_blank' : ''}`} className="eventlist-title-link">{title}</a> : title }
                    </h1>

                    <ul className="eventlist-meta event-meta">
                        <li className="eventlist-meta-item eventlist-meta-address event-meta-item">
                            {location}
                            <a
                                href={`http://maps.google.com?q=${location}`}
                                className="eventlist-meta-address-maplink"
                                target="_blank">(map)</a>
                        </li>
                    </ul>

                </div>
            </div>
            {url && <a href={url} target="_blank" className="tickets">Tickets</a>}
        </article>
    );
};

EventItem.propTypes = {
    url: PropTypes.string,
    location: PropTypes.string,
    title: PropTypes.string,
    externalLink: PropTypes.bool,
    // date: PropTypes.oneOf([PropTypes.string, Date])
};
export default EventItem;