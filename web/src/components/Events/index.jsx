import React, { Component } from 'react';
import './Events.scss';
const Events = ({ children }) => (
    <section id="events" className="events-section">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 events">
                    <div className="row">
                        <div className="col-12 events-heading">
                            <h2>Upcoming Events</h2>
                        </div>
                        <div className="col-12 events-content">
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

export default Events;